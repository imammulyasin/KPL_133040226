/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package IntraclassCycle;

/**
 *
 * @author Imam Mulyasin
 */
public class CompliantCycle {
    private final int balance;
    // Random Deposit
    private static final int deposit = (int) (Math.random() * 100);
    // Inserted after initialization of required fields
    private static final CompliantCycle c = new CompliantCycle();
    public CompliantCycle() {
        // subtract processing fee
        balance = deposit - 10;
    }
    public static void main(String[] args) {
    System.out.println("The account balance is: " + c.balance);
    }
}
