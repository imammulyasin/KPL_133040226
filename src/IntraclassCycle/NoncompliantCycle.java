/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package IntraclassCycle;

/**
 *
 * @author Imam Mulyasin
 */
public class NoncompliantCycle {
    private final int balance;
    private static final NoncompliantCycle c = new NoncompliantCycle();
    // Random Deposit
    private static final int deposit = (int) (Math.random() * 100);
    public NoncompliantCycle() {
    // Subtract processing fee
        balance = deposit - 10;
    }
    public static void main(String[] args) {
        System.out.println("The account balance is: " + c.balance);
 }
}

/*
Analisis
     * Noncompliant code objek c sudah dibuat tetapi ada tambahan variabel deposit
     * pada konstruktur akan terjadi perhitungan dengan rumus yang sudah ditetapkan.
Kesalahan
     * pembuatan objek dilakukan sebelum semua atribut dibuat
     * sedangkan untuk Compliant code, objek dibuat tidsk sds strinut tabahan ketika semua atribut objek sudah dibuat
===================================
     * perhatikan pendeklarassian variabel
*/