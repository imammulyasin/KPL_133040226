/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package L8.expression_used_in_assertion_must_not_produce_side_effects;

import java.util.ArrayList;

/**
 *
 * @author Imam Mulyasin
 */
public class Latihan8 {
    private ArrayList<String> names;
    
//    void process(int index){
//        assert names.remove(null);
//        //...
//    }
    
    void process(int index){
        boolean nullsRemoved = names.remove(null);
        assert nullsRemoved;
    }
    
}
