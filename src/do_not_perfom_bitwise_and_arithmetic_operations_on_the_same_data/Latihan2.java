/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package do_not_perfom_bitwise_and_arithmetic_operations_on_the_same_data;

/**
 *
 * @author Imam Mulyasin
 */
public class Latihan2 {
    public static void main(String[] args) {
    // Noncompliant code 1
        int x = -50;
        x >>>= 2; // hasil 268435452
        
        
        // Noncompliant code 2
        int x = -50;
        x >>= 2; // hasil -13
        
        // Compliant code solution
	
        int x = -50;
        x /=4;
    }
}
