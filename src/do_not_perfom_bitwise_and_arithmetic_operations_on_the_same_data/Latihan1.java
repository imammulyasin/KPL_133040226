/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package do_not_perfom_bitwise_and_arithmetic_operations_on_the_same_data;

/**
 *
 * @author Imam Mulyasin
 */
public class Latihan1 {
    public static void main(String[] args) {
// Noncompliant code 1
//        int x = 50;
//        x += (x << 2) + 1; // hasil 1256
        
        // Noncompliant code 2
//        int x = 50;
//        int y = x << 2;
//        x += y + 1; // hasil 452
        
        // Compliant code solution
        int x = 50;
        x = 5 * x +1; // hasil 251 
       System.out.println(x); 
   }
}
