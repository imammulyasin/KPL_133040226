/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package do_not_perfom_bitwise_and_arithmetic_operations_on_the_same_data;

/**
 *
 * @author Imam Mulyasin
 */
public class Latihan3 {
    public static void main(String[] args) {
     // Bagian 3
        // Noncompliant code 1
        // b[] is a byte array, initialized to 0xff
//        byte[] b = new byte[] { -1, -1, -1, -1};
//        int result = 0;
//        for(int i=0;i<4;i++){
//            result = ((result << 8) + b[i]);
//        }
        // hasil -16843009
        
        // Noncompliant code 2
//        byte[] b = new byte[] { -1, -1, -1, -1};
//        int result = 0;
//        for(int i=0;i<4;i++){
//            result = ((result << 8) + (b[i] & 0xff));
//        }
        // hasil -1
        
        // Compliant code solution
        byte[] b = new byte[] { -1, -1, -1, -1};
        int result = 0;
        for(int i=0;i<4;i++){
            result = ((result << 8) | (b[i] & 0xff));
        }
        // hasil -1
        System.out.println(result);   
    }
}
