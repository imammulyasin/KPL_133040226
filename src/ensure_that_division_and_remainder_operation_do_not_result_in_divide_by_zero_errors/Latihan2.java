/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ensure_that_division_and_remainder_operation_do_not_result_in_divide_by_zero_errors;

/**
 *
 * @author Imam Mulyasin
 */
public class Latihan2 {
    public static void main(String[] args) {
    // Noncompliant code
//        long num1, num2, result;
//        /* Initialize num1 and num2 */
//        num1 = 2;
//        num2 = 0;
//        result = num1 % num2;
//        System.out.println(result);
        
        // Compliant code solution
        long num1, num2, result;
        /* Initialize num1 and num2 */
        num1 = 2;
        num2 = -1;
        num2 = 0;
        if(num2 == 0){
            System.out.println("num2 bernilai 0");
        }else{
            result = num1 / num2;
            result = num1 % num2;
            System.out.println(result);
        }
    }
}
