/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DoNotModifyTheCollectionsElementsDuringanEnhancedforStatements;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Imam Mulyasin
 */
public class T1_3_noncompliantCode {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(new Integer[] {13,14,15});
        boolean first = true;
        
        System.out.println("Processing List...");
        //Noncompliant Code
//        for(Integer i : list){
//            first = false;
//            i = new Integer(99);
//            System.out.println("New Item: "+i);
//        }
        
        //Compliant COde Solution
        for(final Integer i : list){
            Integer item = i;
            if(first){
                first = false;
                item = new Integer(99);
            }
            System.out.println("New item : "+item);
        }
        
        System.out.println("Modified list ?");
        for(Integer i: list){
            System.out.println("List item : "+i);
        }
    }
}
