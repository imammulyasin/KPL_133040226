/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package L7.do_not_use_equality_operators_when_comparing_values_of_boxed_primitives;

/**
 *
 * @author Imam Mulyasin
 */
public class Wrapper {
    public static void main(String[] args) {
//        Integer i1 = 100;
//        Integer i2 = 100;
//        Integer i3 = 1000;
//        Integer i4 = 1000;
//        System.out.println(i1 == i2); // true
//        System.out.println(i1 != i2); // false 
//        System.out.println(i3 == i4); // false
//        System.out.println(i3 != i4); // true
        
        //Compliant code solution
        Integer i1 = 100;
        Integer i2 = 100;
        Integer i3 = 1000;
        Integer i4 = 1000;
        System.out.println(i1.equals(i2)); // true
        System.out.println(!i1.equals(i2)); // false
        System.out.println(i3.equals(i4)); // true
        System.out.println(!i3.equals(i4)); // false
    }
}
