/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package do_not_use_the_object_equals_method_to_compare_two_arrays;

/**
 *
 * @author Imam Mulyasin
 */
public class L6_quals_object {
    public static void main(String[] args) {
        // Noncompliant code
        int[] arr1 = new int[20]; // Initialized to 0
        int[] arr2 = new int[20]; // Initialized to 0
       System.out.println(arr1.equals(arr2)); // Print false
        
//        Compliant code solution
//        int[] arr1 = new int[20]; // Initialized to 0
//        int[] arr2 = new int[20]; // Initialized to 0
//        System.out.println(Arrays.equals(arr1, arr2)); // Print true
        
    }
}
