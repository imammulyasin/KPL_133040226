/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DoNotReusePublicIdentifiersFromTheJavaStandardLibrary;

import java.util.Vector;

/**
 *
 * @author Imam Mulyasin
 */
public class VectorUser {
    public static void main(String[] args) {
        Vector v = new Vector();
        if (v.isEmpty()) {
            System.out.println("Vector is empty");
            /*kesalahan yang mungkin terjadi ketika kita akan memanggil fungsi dari librari vector,
            tetapi kita juga membuat clas dengan nama vector, maka akan terjadi error*/
        }
    }
}
