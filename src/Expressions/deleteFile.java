/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Expressions;

import java.io.File;

/**
 *
 * @author Imam Mulyasin
 */
public class deleteFile {
//Noncompliant Code
//    public void deleteFile(){
//        File someFile = new File("someFileName.txt");
//        // do something with file
	
//        someFile.delete();
//    }
    
    //Compliant Code Solution
    public void deleteFile(){
        File someFile = new File("someFileName.txt");
        // do something with file
        if(!someFile.delete()){
            System.out.println("File gagal untuk dihapus...");
        }
    }
}
