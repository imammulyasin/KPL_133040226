/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul1.do_not_use_equality_operators_when_comparing_values_of_boxed_primitives;

import java.util.ArrayList;

/**
 *
 * @author Imam Mulyasin
 */
public class Wrapper {
    public static void main(String[] args) {

        // BAGIAN 1
        System.out.println("---Latihan Bagian 1---");
        
//        Integer i1 = 100;
//        Integer i2 = 100;
//        Integer i3 = 1000;
//        Integer i4 = 1000;
//        System.out.println(i1 == i2); // true
//        System.out.println(i1 != i2); // false 
//        System.out.println(i3 == i4); // false
//        System.out.println(i3 != i4); // true
        
        //Compliant code solution
        Integer i1 = 100;
        Integer i2 = 100;
        Integer i3 = 1000;
        Integer i4 = 1000;
        System.out.println(i1.equals(i2)); // true
        System.out.println(!i1.equals(i2)); // false
        System.out.println(i3.equals(i4)); // true
        System.out.println(!i3.equals(i4)); // false
        
        /* Kesalahan yang terjadi adalah :
         * kode diatas membandingkan sebuah integer dengan integer lainnya akan tetapi terdapat kesalahan
         * pada saat membandingkan i3 dengan i4 menghasillkan nilai false, kenapa? karena operator pembanding == 
         * membandingkan referensi objek bukan nilai dari objek, selain itu tipe data integer pada Java virutal machine
         * hanya menjamin cache nilai integer dari -127 sampai 127 jadi ketika nilai yang dibandingkan lebih dari 127 
         * maka akan terjadi kesalahan.
         * oleh karena itu pada compliant code solution kita menggunakan fungsi equals untuk membandingkan 2 variabel tersebut
         *
         * Kesimpulan :
         * membandingkan tipe data primitif integer bisa saja menggunakan operator == tetapi hanya nilai -127 sampai dengan 127
         * diluar itu kita harus menggunakan fungsi equals() untuk embandingkannya
         */
        

        
        // BAGIAN 2
        // Noncompliant code
//        System.out.println("---Latihan Bagian 2---");
//        ArrayList<Integer> list1 = new ArrayList<Integer>();
//        for(int i = 0; i < 10; i++){
//            list1.add(i+1000);
//        }
//        
//        ArrayList<Integer> list2 = new ArrayList<Integer>();
//        for(int i = 0; i < 10; i++){
//            list2.add(i+1000);
//        }
//        
//        int counter = 0;
//        for(int i = 0; i < 10; i++){
//            if(list1.get(i) == list2.get(i)){
//                counter++;
//            }
//        }
        
//        System.out.println(counter); // 0
        
        // Compliant Code Solution
        System.out.println("------Latihan Bagian 2------");
        ArrayList<Integer> list1 = new ArrayList<Integer>();
        for(int i = 0; i < 10; i++){
            list1.add(i+1000);
        }
        
        ArrayList<Integer> list2 = new ArrayList<Integer>();
        for(int i = 0; i < 10; i++){
            list2.add(i+1000);
        }
        
        int counter = 0;
        for(int i = 0; i < 10; i++){
            if(list1.get(i).equals(list2.get(i))){
                counter++;
            }
        }
        System.out.println(counter);
        
        /* Kesalahan yang terjadi :
         * sama seeperti pada bagian 1 disini pada codingan Noncompliant kita membandingkan nilai dari setiap array
         * menggunakan operator '==' dimana seperti yang sudah kita ketahui bahwa perbandingan menggunakan operator tersebut
         * hanya bisa digunakan integer dari -127 sampai dengan 127 diluar itu hasilnya error.
         * maka dari itu pada Compliant code solution kita menggunakan fungsi equals untuk membandingkannya.
         */
        
        
       
        Wrapper wr = new Wrapper();
        wr.exampleEqualOperator();
    }
    // BAGIAN 3
    // Noncompliant code
//    public void exampleEqualOperator(){
//        System.out.println("---Latihan Bagian 3---");
//        Boolean b1 = new Boolean("true");
//        Boolean b2 = new Boolean("true");
//        
//        if(b1 == b2){
//            System.out.println("Never printed");
//        }
//    }
    
    // Compliant code solution
    public void exampleEqualOperator(){
        System.out.println("------Latihan Bagian 3------");
        Boolean b1 = true;
        Boolean b2 = true;
        
        if(b1 == b2){
            System.out.println("Always Printed");
        }
        
        b1 = Boolean.TRUE;
        if(b1 == b2){
            System.out.println("Always Printed");
        }
    }
    
    /* Kesalahan yang terjadi :
     * dalam Noncomplaint code konstruktor untuk kelas boolean mengembalikan nilai berbeda, objek yang baru dipakai.
     * menggunakan operator == disini akan menghasilkan nilai yang tidak diharapkan. solusi untuk masalah ini adalah
     * pada pendefinisian variabel boolean kita tidak menggunakan new Boolean(); tetapi langsung saja dengan nilai boolean tersebut
     * sehingga ketika menggunakan operator == kita mendapatkan hasil yang diinginkan.
     */
}
